<?php
/**
 * Created by PhpStorm.
 * User: gandza
 * Date: 11/27/13
 * Time: 12:42 PM
 */

namespace Arilas\ORMTest\Test;

use Arilas\ORM\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class TestParent
 * @package Arilas\ORMTest\Test
 * @ORM\Entity
 * @ORM\Table(name="test_parent")
 */
class TestParent implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", unique=true)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @ORM\ManyToOne(targetEntity="Arilas\ORMTest\Test\Test")
     */
    public $test;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * @param mixed $test
     */
    public function setTest($test)
    {
        $this->test = $test;
    }

}

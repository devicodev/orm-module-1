<?php
/**
 * User: granted
 * Date: 1/5/15
 * Time: 5:48 PM
 */

namespace Arilas\ORM\Mapping;


use Krona\Common\Mapping\ClassMetadataInterface;

class ClassMetadata extends \Doctrine\ORM\Mapping\ClassMetadata implements ClassMetadataInterface
{

    public function isAssociationIsInnerObject($fieldName)
    {
        return false;
    }
}
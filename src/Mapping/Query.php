<?php
/**
 * User: krona
 * Date: 10/28/14
 * Time: 5:04 PM
 */

namespace Arilas\ORM\Mapping;

/**
 * Class Query
 * @package Arilas\ORM\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Query
{
    public $findBy = [];
}